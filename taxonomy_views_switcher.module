<?php
/**
 * @file
 * Module which allows taxonomy term pages to be displayed using different
 * views depending on the request path.
 */

/**
 * Implementation of Hook Menu.
 */
function taxonomy_views_switcher_menu_alter(&$items) {
  $items['taxonomy/term/%'] = array(
    'page callback' => 'taxonomy_views_switcher_render_view',
    'page arguments' => array(2),
    'access callback'  => 'taxonomy_views_switcher_view_access',
    'access arguments' => array(2),
  );

  $items['taxonomy/term/%/%'] = $items['taxonomy/term/%'];
  $items['taxonomy/term/%/'] = $items['taxonomy/term/%'];
  $items['vid/%'] = $items['taxonomy/term/%'];
  $items['tid/%'] = $items['taxonomy/term/%'];
  $items['switch/%'] = $items['taxonomy/term/%'];
  $items['switch/%/%'] = $items['taxonomy/term/%'];
}

/**
 * Implementation of Hook Access.
 */
function taxonomy_views_switcher_view_access($term) {
  // TODO: add access control if necessary.
  return TRUE;
}

/**
 * Renders view or the standard term page if no suitable view found.
 */
function taxonomy_views_switcher_render_view($tid = '', $depth = 10, $op = 'page') {

  $target = taxonomy_views_switcher_get_view();
  if (isset($target['view'])) {

    $view = views_get_view($target['view']);
    $view->set_display($target['display']);
    if (isset($target['tid'])) {
      $view->set_arguments(array($target['tid'], $depth));
    }

    $view->build();
    drupal_set_title(str_replace('&amp;', '&', $view->get_title()));

    return $view->preview();
  }

  // If no suitable view is found, use core drupal taxonomy/term/% page.
  module_load_include('inc', 'taxonomy', 'taxonomy.pages');
  return taxonomy_term_page(taxonomy_term_load($tid));
}

/**
 * Finds an appropriate view to show.
 */
function taxonomy_views_switcher_get_view() {
  // Use request path, not the aliased path and escape the forward slashes.
  $view_path = request_path();

  if ($cached = cache_get("taxonomy_views_switcher:$view_path")) {
    return $cached->data;
  }

  // Default target view.
  $matched_view = array();
  $matched_view_priority = -1;

  if (is_numeric(arg(2))) {
    $tid = arg(2);
  }
  else {
    $real_path = drupal_lookup_path('source', $view_path);
    $ary_path = explode('/', $real_path);
    $tid = $ary_path[2];
  }

  // Loop through all views.
  $views = views_get_all_views();
  foreach ($views as $view) {

    // Ignore view if is disabled.
    if (isset($view->disabled) && $view->disabled) {
      continue;
    }

    // Loop through each display in the view.
    foreach ($view->display as $name => $display) {

      // If the view has a path set.
      $plugin = $display->display_plugin;
      if (!in_array($plugin, array('block', 'feed', 'default'))) {
        if (!isset($display->display_options['path'])) {
          continue;
        }

        /* For each path in the view (delimited by |)
         * note: multiple paths delimited by | is a TVS 'hack'.
         * not standard views behavior. */
        $paths = explode('|', $display->display_options['path']);
        foreach ($paths as $i => $path) {
          // Is this a matching path.
          $priority = taxonomy_views_switcher_match_path($view_path, $path, $tid);
          // If priority is higher than previously matched then save it.
          if ($priority > $matched_view_priority) {
            $matched_view = array(
              'tid' => $tid,
              'view' => $view->name,
              'display' => $name,
              'path' => $path,
            );
            $matched_view_priority = $priority;
          }

          if ($priority == 3) {
            // Highest priority (exact match) found, so no need to continue.
            break;
          }
        }
      }
    }
  }

  $matched_view['tid'] = $tid;
  cache_set("taxonomy_views_switcher:$view_path:", $matched_view);
  return $matched_view;
}

/**
 * Tests wether a view display matches the current path.
 */
function taxonomy_views_switcher_match_path($view_path, $path, $tid) {

  /* Remove "switch" from path for backwards compatibilty with earlier
  version of this module. */
  $path = preg_replace('/^(switch)?\//', '', $path);

  // Set default view - if default taxonomy/term view is found.
  if (substr($path, 0, 14) == 'taxonomy/term/') {
    // Lowest priority match.
    return 0;
  }

  // Set target view - match the view to the current request_path
  // this function probably deprecated.
  if (preg_match('/^' . str_replace('/', '\/', $path) . '/', $view_path)) {
    /* This path matches, now we want to see if it's a more specific
    (ie deeper) path than any existing ones). */
    return substr_count($path, '/');
  }
  // See if there is a path matching taxonomy vid/machine_name.
  elseif (substr($path, 0, 4) == 'vid/') {
    $term = taxonomy_term_load($tid);
    // Match vid or vocabulary machine name.
    $valid_vid = FALSE;
    if ($path == 'vid/' . $term->vid
        || $path == 'vid/' . $term->vocabulary_machine_name) {
      return 2;
    }
  }
  // See if there is a path matching taxonomy tid/term name.
  elseif (substr($path, 0, 4) == 'tid/') {
    $term = taxonomy_term_load($tid);
    // Match tid or taxonomy term name.
    if ($path == 'tid/' . $term->tid || $path == 'tid/' . $term->name) {
      return 3;
    }
  }

  // No match.
  return -1;
}
