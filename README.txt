Taxonomy View Switcher Overview

This module allows different views to be shown for different taxonomy term pages
It is similar in purpose to Taxonomy_Views_Integation but views are linked
to terms and vocabularies via Views admin rather than Taxonomy Admin

-----------------------------------------------------------------------------
Usage:

1: Enable Taxonomy View Switcher module (requires taxonomy and views)

2. Enable the default "Taxonomy Term" view (and/or clone this view if you like)

3. Taxonomy View Switcher will now use the first view it finds with a path of 
  'taxonomy/term/%' as the default view for any term page. 
  (This mirrors the standard Views behaviour)
  
4. You can now create views with paths such as those shown below and
   Taxonomy Views Switcher will ensure the correct view will be shown
   for the corresponding term page:
  
    Vocabulary Matching  (matches all terms in a vocabulary)
      vid/5
      vid/taxonomy_machine_name
      
    Term Matching (matches a particular term)
      tid/43
      tid/dowloads
      
    Term Matching with depth (matches a term and child terms to the given depth)
    (for info only - not yet implemented)
      tid/43/0
      tid/dowloads/4

    Note that multiple paths can be specified using a | character, so you
    can have:
      vid/5|vid/6
      tid/43|tid/44|tid/45
      vid/5|tid/44
  
  It is also currently still possible to use "path matching", which allows
  views to be matched to terms based on the "friendly url" of the term, but
  this method is only for backwards compatibility it's recommended to use the
  tid or vid paths.
  
      switch/products
      switch/products/subcategory
      switch/news
      
  The 'switch' part identifies the view to Taxonomy View Switcher and prevents
  conflicts with any other urls. 


--------------------------------------------------------------------------------
Theming

There are no theming options as Taxonomy View Switcher only redirects output
to the relevant view.

--------------------------------------------------------------------------------
Notes:

1: This module is still at a very experimental stage. Take care when upgrading
   to test that any functionality that you rely on has not been lost before
   you delete the previous version of the module code.
   
2. If you use this module, please leave some feedback on the module project page
   I get *nothing* for working on this module other than the satisfaction for
   helping out my fellow Drupalistas. 

3: Taxonomy View Switcher has not been tested with multiple term displays. 
   ex. taxonomy/term/4+6+7 but it might work if you try it.

3: Taxonomy View Switcher does not care what your view does however TVS will
   pass the term id and term id with depth modifier to the view as arguments.
   To make use of these, simply add the following arguments to the view you
   plan to use on your term or vocabulary:
   
   A1: Taxonomy: Term ID (with depth)
   A2: Taxonomy: Term ID depth modifier
   
4: Taxonomy View Switcher only looks for "page" displays

5: Taxonomy View Switcher uses hook_menu_alter to replace the taxonomy/term/*
   callback. Unlike the re-direct method, which causes your server to be
   queried 2 or more times for each request, the hook_menu_alter method
   conserves your server resources. 

6. Taxonomy View Switcher is based significantly on the Drupal 7 port of
   TVI: Taxonomy Views Integration but is simpler and to me works in more
   intuitive way. 
   
----------------------------------------------------------------------------
